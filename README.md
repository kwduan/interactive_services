# Interactive Services #

This project aims to provide a mechanism to allow human intervention/manipulation in a scientific workflow. Currently, it aims to integrate with Taverna (http://www.taverna.org.uk/) workflow management system. However, because it is entirely built based on REST web service, so it can be integrated into any types of scientific workflows that allow REST web service access. Also with the support of client programs, it can be integrated with any workflow that can access local command line based programs.

### Functions ###
#### Task initialisation service ####
 Users can use this service to initialise an interactive task. The information about this task will be stored in a worklist on server side.
#### Task notification service ####
 Based on the information gathered from task initialisation service, this service can notify corresponding users for further intervention on this task. Notification is sent through E-mail. The E-mail includes the corresponding webpage/web service interfaces for interaction.
#### Task status detection service ####
 This service allows users to check the status of an interactive task. In the case it has been processed through the webpage and web service provided from last service. The workflow can carried out automatically. 

### Installation and Configuration
The information can be found at [**here**](deployment.md).

### Usages
The information about web service interface, web pages and clients can be found at [**here**](apis.md).

### Contacts ###
This service is still in development. If you are interested in or have any questions, please contact k.duan@sheffield.ac.uk