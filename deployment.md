#Interactive Service Deployment

###Prerequisite
The whole deployment is tested under a clean Ubuntu 14.04 LTS. 

###Installation
####1. Install dependencies from apt-get

#####Install git
	sudo apt-get install git
#####Install pip
	sudo apt-get install python-pip
#####Install ffi
	sudo apt-get install libffi6 libffi-dev

####2. Clone git repository
			
	git clone https://bitbucket.org/me1kd/interactive_services.git
	
####3. Install dependencies from pip
#####Install virtual environment for Python
	sudo pip install virtualenv
#####Start virtual environment
   There is already an env folder created in repository for test and demo purposes.
   User can create their own. Here the demo one will be used.

    source <repo_root>/env/bin/activate
#####Upgrade tools for Python			
	sudo pip install -upgrade setuptools pip
#####Install Python packageds
	sudo pip install -r <repo_root>/requirements.txt

###Configuration
####1. Configuration for service and SMTP
#####Copy the conf.py template for service and SMTP to designated folder
	cp <repo_root>/conf_temp/conf\(imageIteractive\).py <repo_root>/imageInteractive/conf.py

There are some fields should be filled by users. "smtpServer" is the address of 
your SMTP server. In the template a gmail account is used. Its username and password 
should be filled. 

"MAIL_TEMPLATE" is the content template for the email you are going to send to the
approver when there is an intervention work task created. Placeholders can be used
here. In the template, {viewerType} and {workid} are used. They are replaced by the 
real values in **get** function of class **WorkCreater** in **imageinteractive/views.py**

"URL_ROOT" is the root address for your service.

####2. Configuration for customized authentication service
#####Copy the conf.py template for customized authentication service
	cp <repo_root>/conf_temp/conf\(interactiveService\).py <repo_root>/interactiveService/conf.py

This is just for users who want to use their own customized authentication service. 
Here in the example, a remote PostgresSQL database is used for this purpose. Thus, field DEFAULT_AUTHENTICATION_CLASSES is added with customized authentication class 
interactiveService.customize_auth.PostgresAuthentication. User can use only local 
authentication service provided by django. In such a case, this line can be commented out.

Inside the conf.py, there are four fields to fill: hostname, username, password 
and database.

###Start the service
The simplest way to start this service is by command:
		
	python manage.py runserver 0.0.0.0:8000
Also, it can be deployed in Apache, more information can be found [here](https://docs.djangoproject.com/en/1.9/howto/deployment/wsgi/modwsgi/).