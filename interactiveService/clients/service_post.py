import requests, sys, json
from requests.auth import HTTPBasicAuth

def post(URL, username, password):
    url = URL
    auth = HTTPBasicAuth(username, password)
    r = requests.post(url=url, auth=auth)
    try:
        json_data = json.loads(r.text)
        sys.stdout.write(str(json_data['workid']))
        sys.stdout.flush()
        return r.status_code
    except:
        return r.status_code

URL = sys.argv[1]
username = sys.argv[2]
password = sys.argv[3]
post(URL, username, password)