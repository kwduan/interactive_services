import requests, sys, json
from requests.auth import HTTPBasicAuth

def get(URL, username, password):
    url = URL
    auth = HTTPBasicAuth(username, password)
    r = requests.get(url=url, auth=auth)
    try:
        json_data = json.loads(r.text)
        print json_data['Approvement Status']
        return r.status_code
    except:
        json_data = json.loads(r.text)
        sys.stdout.write(str(json_data['workid']))
        sys.stdout.flush()
        return r.status_code

URL = sys.argv[1]
username = sys.argv[2]
password = sys.argv[3]
get(URL, username, password)