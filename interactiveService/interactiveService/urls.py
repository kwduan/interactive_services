from django.conf.urls import include, url

from django.contrib import admin
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.conf.urls.static import static
admin.autodiscover()
from . import settings

urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^image/', include('imageInteractive.urls')),
]

urlpatterns += staticfiles_urlpatterns()
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
