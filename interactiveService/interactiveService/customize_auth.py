from django.contrib.auth.models import User
import psycopg2
from . import conf
import passlib.hash
from rest_framework import authentication
import base64


def doQuery(conn, queryname):
    cur = conn.cursor()
    query = "SELECT username, email, password FROM public.user WHERE username='" + queryname + "'"
    cur.execute(query)
    for username, email, password in cur.fetchall():
        user = User(username=username, password=password)
        user.email = email
        return user


def getUser(queryname):
    myConnection = psycopg2.connect(host=conf.hostname, user=conf.username, password=conf.password,
                                    dbname=conf.database)
    try:
        user = doQuery(myConnection, queryname)
        myConnection.close()
        return user
    except:
        myConnection.close()
        return None


class PostgresAuthentication(authentication.BaseAuthentication):
    def authenticate(self, request):
        try:
            auth_header = request.META['HTTP_AUTHORIZATION']
            encoded_credentials = auth_header.split(' ')[1]  # Removes "Basic " to isolate credentials
            decoded_credentials = base64.b64decode(encoded_credentials).decode("utf-8").split(':')
            username = decoded_credentials[0]
            password = decoded_credentials[1]

            user = getUser(username)
            login_valid = (user is not None)
            pwd_valid = passlib.hash.bcrypt.verify(password, user.password)
            if login_valid and pwd_valid:
                return user, None
            else:
                return None
        except:
            raise authentication.exceptions.AuthenticationFailed

    def authenticate_header(self, request):
        return "Basic" #By overriding authenticate_header, method responses with 401 and "Basic" for WWW-Authenticate