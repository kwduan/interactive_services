__author__ = 'Kewei Duan'

from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^email/(?P<username>\w+)$', views.WorkCreater.as_view()),
    url(r'^viewer/', views.ImageManager.as_view()),
    url(r'^pdfviewer/', views.PdfManager.as_view()),
    url(r'^decision/', views.DecisionProber.as_view()),
    url(r'^upload/', views.ImageUploader.as_view()),
    url(r'^worktodo/', views.WorkLists.as_view())
]

