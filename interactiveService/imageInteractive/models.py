__author__ = 'Kewei Duan'

from django.db import models


class Image(models.Model):
    id = models.AutoField(primary_key=True)
    title = models.CharField(max_length=250, help_text='Maximum 250 characters.')
    creatingUser = models.CharField(max_length=50, blank=False)
    imgFile = models.FileField(blank=True) #file will be saved in MEDIA_ROOT
    mimeType = models.CharField(max_length=50)

    def __str__(self):
        return self.title


class WorkList(models.Model):
    id = models.AutoField(primary_key=True)
    image = models.ForeignKey('Image', on_delete=models.CASCADE, related_name='image')
    uploadData = models.ForeignKey('Image', on_delete=models.CASCADE, related_name='uploadData', blank=True, null=True)
    isApproved = models.IntegerField(default=0)
    created_date = models.DateTimeField(auto_now_add=True)
    modified_date = models.DateTimeField(auto_now=True)
    examiner = models.CharField(max_length=50, blank=False)

    def __str__(self):
        return str(self.id)

