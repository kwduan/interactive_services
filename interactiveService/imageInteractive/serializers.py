from . import models
from rest_framework import serializers


class WorklistsSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.WorkList
        fields = '__all__'