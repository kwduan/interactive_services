__author__ = 'Kewei Duan'

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework.renderers import JSONRenderer
from django.shortcuts import render_to_response
from django.core.exceptions import ObjectDoesNotExist
import requests, shutil, os
from . import conf, models, forms, utils, serializers
import sys

sys.path.append(os.path.dirname(os.path.abspath(__file__)) + '/../interactiveService')
from interactiveService import customize_auth
import magic

BASE_DIR = os.path.dirname(os.path.dirname(__file__))
MEDIA_ROOT = os.path.join(BASE_DIR, 'image/')


class WorkCreater(APIView):
    """The class to create worklist item from an image"""

    def get(self, request, username):
        """
        :param request:
        :param username: string from URL section
        :return response:
        """
        try:
            temp_user = request.user
        except:
            return Response(status=status.HTTP_404_NOT_FOUND)
        recipient_user = customize_auth.getUser(username)

        recipient = recipient_user.email
        email_subject = "test"
        media_id = request.GET.get('id')
        body = conf.MAIL_TEMPLATE

        #Decide which viewer to use by mimetype of the content
        img = models.Image.objects.get(id=int(media_id))
        mime_type = img.mimeType
        if mime_type == "application/pdf":
            body = body.replace("{viewerType}", "pdfviewer")
        elif mime_type == "image/jpg":
            body = body.replace("{viewerType}", "viewer")

        if img.creatingUser == temp_user.username:
            worklist = models.WorkList(image=img, examiner=temp_user)
            worklist.save()
            id = worklist.id
            body = body.replace("{workid}", str(id))
            resp_body = {"workid": id}
            utils.send_mail(conf.smtpServer, conf.gmail_username, conf.gmail_password, recipient, email_subject, body)
            return Response(data=resp_body, status=status.HTTP_200_OK)
        else:
            return Response(status=status.HTTP_401_UNAUTHORIZED)


class ImageManager(APIView):
    """The class to generate html pages for images verification"""

    def get(self, request):
        """
        :param request:
        :return response:
        """
        worklist_id = request.GET.get('workid')
        worklist = models.WorkList.objects.get(id=int(worklist_id))
        request_user = request.user.username
        assigned_examiner = worklist.examiner
        if request_user != assigned_examiner:
            return Response(status=status.HTTP_403_FORBIDDEN)
        temp_img = worklist.image
        # username = temp_img.creatingUser
        # title = temp_img.title
        form = forms.checkboxForm(request.POST or None)
        try:
            img = temp_img
            img_url = conf.URL_ROOT + "media/" + img.imgFile.name
            return render_to_response('imageViewer.html', {'img_url': img_url, 'form': form})
        except ObjectDoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

    def post(self, request):
        """
        :param request:
        :param response:
        """
        worklist_id = request.GET.get('workid')
        worklist = models.WorkList.objects.get(id=int(worklist_id))
        request_user = request.user.username
        assigned_examiner = worklist.examiner
        if request_user != assigned_examiner:
            return Response(status=status.HTTP_403_FORBIDDEN)
        temp_img = worklist.image
        user_choice = request.POST["examination_decision"]
        if user_choice == "yes":
            try:
                worklist = models.WorkList.objects.get(id=int(worklist_id))
                worklist.isApproved = 1
                worklist.save(update_fields=["isApproved"])
                img_url = conf.URL_ROOT + "media/" + temp_img.imgFile.name
                return render_to_response('imageConfirmation.html', {'img_url': img_url})
            except ObjectDoesNotExist:
                return Response(status=status.HTTP_404_NOT_FOUND)
        elif user_choice == "no":
            try:
                worklist = models.WorkList.objects.get(id=int(worklist_id))
                worklist.isApproved = 2
                worklist.save(update_fields=["isApproved"])
                img_url = conf.URL_ROOT + "media/" + temp_img.imgFile.name
                return render_to_response('imageConfirmation.html', {'img_url': img_url})
            except ObjectDoesNotExist:
                return Response(status=status.HTTP_404_NOT_FOUND)
        else:
            return Response(status=status.HTTP_406_NOT_ACCEPTABLE)


class PdfManager(APIView):
    """The class to generate html pages for PDFs verification"""

    def get(self, request):
        """
        :param request:
        :return response:
        """
        worklist_id = request.GET.get('workid')
        worklist = models.WorkList.objects.get(id=int(worklist_id))
        request_user = request.user.username
        assigned_examiner = worklist.examiner
        if request_user != assigned_examiner:
            return Response(status=status.HTTP_403_FORBIDDEN)
        temp_img = worklist.image
        # username = temp_img.creatingUser
        # title = temp_img.title
        form = forms.checkboxForm(request.POST or None)
        try:
            img = temp_img
            img_url = conf.URL_ROOT + "media/" + img.imgFile.name
            return render_to_response('pdfViewer.html', {'pdf_url': img_url, 'form': form})
        except ObjectDoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

    def post(self, request):
        """
        :param request:
        :param response:
        """
        worklist_id = request.GET.get('workid')
        worklist = models.WorkList.objects.get(id=int(worklist_id))
        request_user = request.user.username
        assigned_examiner = worklist.examiner
        if request_user != assigned_examiner:
            return Response(status=status.HTTP_403_FORBIDDEN)
        temp_img = worklist.image
        user_choice = request.POST["examination_decision"]
        if user_choice == "yes":
            try:
                worklist = models.WorkList.objects.get(id=int(worklist_id))
                worklist.isApproved = 1
                worklist.save(update_fields=["isApproved"])
                img_url = conf.URL_ROOT + "media/" + temp_img.imgFile.name
                return render_to_response('pdfConfirmation.html', {'pdf_url': img_url})
            except ObjectDoesNotExist:
                return Response(status=status.HTTP_404_NOT_FOUND)
        elif user_choice == "no":
            try:
                worklist = models.WorkList.objects.get(id=int(worklist_id))
                worklist.isApproved = 2
                worklist.save(update_fields=["isApproved"])
                img_url = conf.URL_ROOT + "media/" + temp_img.imgFile.name
                return render_to_response('imageConfirmation.html', {'img_url': img_url})
            except ObjectDoesNotExist:
                return Response(status=status.HTTP_404_NOT_FOUND)
        else:
            return Response(status=status.HTTP_406_NOT_ACCEPTABLE)


class DataManager(APIView):
    """The class to generate html pages for PDFs verification"""

    def get(self, request):
        """
        :param request:
        :return response:
        """
        worklist_id = request.GET.get('workid')
        worklist = models.WorkList.objects.get(id=int(worklist_id))
        request_user = request.user.username
        assigned_examiner = worklist.examiner
        if request_user != assigned_examiner:
            return Response(status=status.HTTP_403_FORBIDDEN)
        temp_data = worklist.image
        # username = temp_img.creatingUser
        # title = temp_img.title
        form = forms.checkboxForm(request.POST or None)
        try:
            data = temp_data
            data_url = conf.URL_ROOT + "media/" + data.imgFile.name
            return render_to_response('dataManage.html', {'data_url': data_url})
        except ObjectDoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

    def post(self, request): #TODO: needs to be tested
        """
        :param request:
        :param response:
        """
        title = request.GET.get('title')
        username = request.user.username
        worklist_id = request.GET.get('workid')
        worklist = models.WorkList.objects.get(id=int(worklist_id))
        request_user = request.user.username
        assigned_examiner = worklist.examiner
        file_name = username + '_' + title + '_uploaded'
        if request_user != assigned_examiner:
            return Response(status=status.HTTP_403_FORBIDDEN)

        file = request.FILES['uploadFile']
        image = models.Image(title=title, creatingUser=username)
        with open(MEDIA_ROOT + file_name, 'wb') as f:
            shutil.copyfileobj(file, f)
            mime = magic.Magic(mime=True)
            mime_type = mime.from_file(MEDIA_ROOT + file_name)
        with open(MEDIA_ROOT + file_name, 'rb') as f:
            image.imgFile = file_name
            image.mimeType = mime_type
            image.save()
        data_id = image.id
        uploaded_data = models.Image.objects.get(id=data_id)
        worklist.uploadData = uploaded_data
        worklist.isApproved = 1
        worklist.save(update_fields=["uploadData","isApproved"])
        #data_url = conf.URL_ROOT + "media/" + image.imgFile.name
        return render_to_response('dataConfirmation.html', {'data_name': title})


class DecisionProber(APIView):
    """The class to check if an image is verified."""
    renderer_classes = (JSONRenderer,)

    def get(self, request):
        """
        :param request:
        :return response:
        """
        worklist_id = request.GET.get('workid')
        worklist = models.WorkList.objects.get(id=int(worklist_id))
        temp_img = worklist.image
        title = temp_img.title
        if worklist.isApproved == 0:
            img_text = 'Not approved yet.'
        elif worklist.isApproved == 1:
            img_text = "Accepted"
        elif worklist.isApproved == 2:
            img_text = 'Not Accepted.'
        result = {'Image Title': title, 'Approvement Status': worklist.isApproved, 'Approvement Comment': img_text}
        return Response(data=result, status=status.HTTP_200_OK)


class ReturnedDataRetriever(APIView): #TODO: to be tested

    def get(self, request):
        worklist_id = request.GET.get('workid')
        worklist = models.WorkList.objects.get(id=int(worklist_id))
        temp_data = worklist.uploadData
        try:
            data = temp_data
            data_url = conf.URL_ROOT + "media/" + data.imgFile.name
            resp_body = {"data_url": data_url}
            return Response(data=resp_body, status=status.HTTP_200_OK)
        except ObjectDoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

class WorkLists(APIView):
    """The class to get all the worklists with isApproved equals to 0(not done)."""
    renderer_classes = (JSONRenderer,)

    def get(self, request):
        """

        :param request:
        :return response:
        """
        worklists = models.WorkList.objects.filter(isApproved=0)
        serializer = serializers.WorklistsSerializer(worklists, many=True)
        return Response(data=serializer.data, status=status.HTTP_200_OK)


class ImageUploader(APIView):
    """The class to upload image from URL of other data repository"""

    def post(self, request):
        """
        :param request:
        :return response:
        """
        title = request.GET.get('title')
        username = request.user.username
        image_url = request.GET.get('data_URL')
        # image repo uses the same auth and returns the raw image data directly,
        resp = requests.get(image_url, auth=utils.fetchAuth(request), stream=True)
        file_name = username + '_' + title
        if resp.status_code == 200:
            image = models.Image(title=title, creatingUser=username)
            with open(MEDIA_ROOT + file_name, 'wb') as f:
                resp.raw.decode_content = True
                shutil.copyfileobj(resp.raw, f)
                mime = magic.Magic(mime=True)
                mime_type = mime.from_file(MEDIA_ROOT + file_name)
                if mime_type != "application/pdf" and type != "image/jpeg":
                    os.remove(MEDIA_ROOT + file_name)
                    return Response(status=status.HTTP_400_BAD_REQUEST)
            with open(MEDIA_ROOT + file_name, 'rb') as f:
                image.imgFile = file_name
                image.mimeType = mime_type
                image.save()
            id = image.id
            resp_body = {"workid": id}
            return Response(data=resp_body, status=status.HTTP_200_OK)
        else:
            return resp
