from django import forms

DISPLAY_CHOICES = (
    ('yes', 'Image Accepted'),
    ('no', 'Image Denied')
)

class checkboxForm(forms.Form):
    examination_decision = forms.ChoiceField(widget=forms.RadioSelect, choices=DISPLAY_CHOICES, label="Examination Decision:")