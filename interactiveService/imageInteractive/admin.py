from django.contrib import admin
from .models import Image, WorkList

class WorkListAdmin(admin.ModelAdmin):
    readonly_fields = ('created_date', 'modified_date',)

# Register your models here.
admin.site.register(Image)
admin.site.register(WorkList, WorkListAdmin)