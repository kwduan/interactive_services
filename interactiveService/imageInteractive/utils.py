import smtplib, json, requests
from requests.auth import AuthBase
from . import conf


def send_mail(server, user, pwd, recipient, subject, body):
    """ function used to generate and send email.
	:param server: string
	:param user: string
	:param pwd: string
	:param recipient: string
	:param subject: string
	:param body: string
	"""
    mail_user = user
    mail_pwd = pwd
    server_address = server
    FROM = user
    TO = recipient if type(recipient) is list else [recipient]
    SUBJECT = subject
    TEXT = body

    # Prepare actual message
    message = """\From: %s\nTo: %s\nSubject: %s\n\n%s
		""" % (FROM, ", ".join(TO), SUBJECT, TEXT)
    try:
        server = smtplib.SMTP(server_address, 587)
        server.ehlo()
        server.starttls()
        server.ehlo()
        server.login(mail_user, mail_pwd)
        server.sendmail(FROM, TO, message)
        server.close()
        print 'Successfully sent the mail.'
    except Exception as e:
        print "Failed to send mail."
        print e.message


def fetchAuth(request):
    """ Generate HTTPBasicAuth instance by username and password strings
	:param request: request
	"""
    auth_str = request.META.get('HTTP_AUTHORIZATION')  # This allows director to act as the user.
    auth = TokenAuth(auth_str)
    return auth


def getImageURL(request):
    """ Generate URL for an uploaded image.
	"""
    title = request.GET.get('title')
    username = request.GET.get('username')
    image_url = conf.REPO_URL + '?title=' + title + "&username=" + username
    resp = requests.get(image_url, auth=fetchAuth(request))
    resp_url = json.loads(resp.content)
    return resp_url['url']


class TokenAuth(AuthBase):
    """
	Inherit from AuthBase to generate JWT based Authorization header field
	"""

    def __init__(self, token):
        self.token = token

    def __call__(self, r):
        r.headers['Authorization'] = self.token
        return r

