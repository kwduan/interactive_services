#This is a configuration file to specify the values may used in a mail sending session.
import os

#SMTP Server address.
smtpServer = ""

#GMAIL is used.
gmail_username = ""
gmail_password = ""

#The tempalte for mail content.
MAIL_TEMPLATE = "http://localhost:8000/image/{viewerType}/?workid={workid}"

#URL_ROOT
URL_ROOT = "http://localhost:8000/"

